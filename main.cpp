#ifdef __APPLE__
#include <GLUT/glut.h>
#endif
#ifdef __unix__
#include <GL/glut.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <memory>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include "GLSL.h"
#include "Program.h"
#include "Camera.h"
#include "MatrixStack.h"
#include "ShapeObj.h"
#include "trimesh/Trimesh.h"

using namespace std;
// using namespace trimesh;

bool keyToggles[256] = {false};

float t = 0.0f;
float tPrev = 0.0f;
int width = 1;
int height = 1;

Program prog;
Camera camera;
ShapeObj bunny;

trimesh::TriMesh* mesh;
unsigned curve1BufID, curve2BufID;
unsigned pDir1BufID, pDir2BufID;
unsigned dCurveBufID;

void loadScene()
{
	t = 0.0f;
	keyToggles['c'] = true;
	
	bunny.load("bunny.obj");
	// bunny.load("dem.obj");
	prog.setShaderNames("simple_vert.glsl", "simple_frag.glsl");

	mesh = trimesh::TriMesh::read("bunny.obj");
	// mesh = trimesh::TriMesh::read("dem.obj");
	// mesh = trimesh::TriMesh::read("bunny2.ply");
	// mesh = trimesh::TriMesh::read("dem.ply");
	mesh->need_curvatures();
	mesh->need_dcurv();

	//TODO: Figure out what is calculating pdir and therefore what pdir is!
}

void initGL()
{
	//////////////////////////////////////////////////////
	// Initialize GL for the whole scene
	//////////////////////////////////////////////////////
	
	// Set background color
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	// Enable z-buffer test
	glEnable(GL_DEPTH_TEST);
	
	//////////////////////////////////////////////////////
	// Intialize the shapes
	//////////////////////////////////////////////////////
	
	bunny.init();

	// Send the first curvature array to the GPU
	const vector<float> &curveBuf1 = mesh->curv1;
	glGenBuffers(1, &curve1BufID);
	glBindBuffer(GL_ARRAY_BUFFER, curve1BufID);
	glBufferData(GL_ARRAY_BUFFER, curveBuf1.size()*sizeof(float), &curveBuf1[0], GL_STATIC_DRAW);

	// Send the second curvature array to the GPU
	const vector<float> &curveBuf2 = mesh->curv2;
	glGenBuffers(1, &curve2BufID);
	glBindBuffer(GL_ARRAY_BUFFER, curve2BufID);
	glBufferData(GL_ARRAY_BUFFER, curveBuf2.size()*sizeof(float), &curveBuf2[0], GL_STATIC_DRAW);

	// Send the derivative curvature array to the GPU??
	// const vector<float> &dCurveBuf = mesh->dcurv;
	glGenBuffers(1, &dCurveBufID);
	glBindBuffer(GL_ARRAY_BUFFER, dCurveBufID);
	glBufferData(GL_ARRAY_BUFFER, mesh->dcurv.size()*4*sizeof(float), &mesh->dcurv[0], GL_STATIC_DRAW);

	// Send the first point direction array to the GPU
	glGenBuffers(1, &pDir1BufID);
	glBindBuffer(GL_ARRAY_BUFFER, pDir1BufID);
	glBufferData(GL_ARRAY_BUFFER, mesh->pdir1.size()*4*sizeof(float), &mesh->pdir1[0], GL_STATIC_DRAW);

	// Send the second point direction array to the GPU
	glGenBuffers(1, &pDir2BufID);
	glBindBuffer(GL_ARRAY_BUFFER, pDir2BufID);
	glBufferData(GL_ARRAY_BUFFER, mesh->pdir2.size()*4*sizeof(float), &mesh->pdir2[0], GL_STATIC_DRAW);
	
	//////////////////////////////////////////////////////
	// Intialize the shaders
	//////////////////////////////////////////////////////
	
	prog.init();
	prog.addUniform("P");
	prog.addUniform("MV");
	prog.addUniform("C");
	prog.addUniform("fSize");
	prog.addAttribute("vertPos");
	prog.addAttribute("vertNor");

	prog.addAttribute("curv1");
	prog.addAttribute("curv2");
	prog.addAttribute("dcurv");
	prog.addAttribute("pdir1");
	prog.addAttribute("pdir2");
	
	//////////////////////////////////////////////////////
	// Final check for errors
	//////////////////////////////////////////////////////
	GLSL::checkVersion();
}

void reshapeGL(int w, int h)
{
	// Set view size
	width = w;
	height = h;
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	camera.setAspect((float)width/(float)height);
}

void drawGL()
{
	// Elapsed time
	float tCurr = 0.001f*glutGet(GLUT_ELAPSED_TIME); // in seconds
	if(keyToggles[' ']) {
		t += (tCurr - tPrev);
	}
	tPrev = tCurr;
	
	// Clear buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if(keyToggles['c']) {
		glEnable(GL_CULL_FACE);
	} else {
		glDisable(GL_CULL_FACE);
	}
	if(keyToggles['l']) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	} else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	
	//////////////////////////////////////////////////////
	// Create matrix stacks
	//////////////////////////////////////////////////////
	
	MatrixStack P, MV;

	// Apply camera transforms
	P.pushMatrix();
	camera.applyProjectionMatrix(&P);
	MV.pushMatrix();
	camera.applyViewMatrix(&MV);
	
	//////////////////////////////////////////////////////
	// Draw origin frame using old-style OpenGL
	// (before binding the program)
	//////////////////////////////////////////////////////
	
	// Setup the projection matrix
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadMatrixf(P.topMatrix().data());
	
	// Setup the modelview matrix
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadMatrixf(MV.topMatrix().data());
	
	// Draw frame
	glLineWidth(2);
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(1, 0, 0);
	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 1, 0);
	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 1);
	glEnd();
	glLineWidth(1);
	
	// Pop modelview matrix
	glPopMatrix();
	
	// Pop projection matrix
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	
	// Draw string
	P.pushMatrix();
	P.ortho(0, width, 0, height, -1, 1);
	glPushMatrix();
	glLoadMatrixf(P.topMatrix().data());
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glRasterPos2f(5.0f, 5.0f);
	char str[256];
	sprintf(str, "Rotation: %s%s%s -> %s%s%s",
			(keyToggles['x'] ? "x" : "_"),
			(keyToggles['y'] ? "y" : "_"),
			(keyToggles['z'] ? "z" : "_"),
			(keyToggles['X'] ? "X" : "_"),
			(keyToggles['Y'] ? "Y" : "_"),
			(keyToggles['Z'] ? "Z" : "_"));
	for(int i = 0; i < strlen(str); ++i) {
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, str[i]);
	}
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	P.popMatrix();
	
	//////////////////////////////////////////////////////
	// Now draw the shape using modern OpenGL
	//////////////////////////////////////////////////////
	
	// Bind the program
	prog.bind();
	
	// Send projection matrix (same for all bunnies)
	glUniformMatrix4fv(prog.getUniform("P"), 1, GL_FALSE, P.topMatrix().data());
	// Eigen::Vector3f cam = camera.getCenter();
	// MV.print("MV");
	Eigen::Vector4f cam = MV.topMatrix() * Eigen::Vector4f(1, 1, 1, 1);
	// cout << "cam: " << cam << endl;
	glUniform3f(prog.getUniform("C"), cam.x(), cam.y(), cam.z());

	glUniform1f(prog.getUniform("fSize"), mesh->feature_size());
	
	// Apply some transformations to the modelview matrix.
	// Each bunny should get a different transformation.
	
	// The center of the bunny is at (-0.2802, 0.932, 0.0851)
	Eigen::Vector3f center(-0.2802, 0.932, 0.0851);
        
	// Alpha is the linear interpolation parameter between 0 and 1
	float alpha = std::fmod(t, 1.0f);
	
	// The axes of rotatio for the source and target bunnies
	Eigen::Vector3f axis0, axis1;
	axis0(0) = keyToggles['x'] ? 1.0 : 0.0f;
	axis0(1) = keyToggles['y'] ? 1.0 : 0.0f;
	axis0(2) = keyToggles['z'] ? 1.0 : 0.0f;
	axis1(0) = keyToggles['X'] ? 1.0 : 0.0f;
	axis1(1) = keyToggles['Y'] ? 1.0 : 0.0f;
	axis1(2) = keyToggles['Z'] ? 1.0 : 0.0f;
	if(axis0.norm() > 0.0f) {
		axis0.normalize();
	}
	if(axis1.norm() > 0.0f) {
		axis1.normalize();
	}
	
	Eigen::Quaternionf q0, q1;
	q0 = Eigen::AngleAxisf(90.0f/180.0f*M_PI, axis0);
	q1 = Eigen::AngleAxisf(90.0f/180.0f*M_PI, axis1);
	
	Eigen::Vector3f p0, p1;
	p0 << -1.0f, 0.0f, 0.0f;
	p1 <<  1.0f, 0.0f, 0.0f;
            
   // Rotation
   Eigen::Matrix4f R = Eigen::Matrix4f::Identity();
   R.block<3,3>(0,0) = q0.toRotationMatrix();
	
   // Draw bunny 1 at left of Origin
	MV.pushMatrix();
      MV.translate(p0);
      MV.multMatrix(R);
	   MV.translate(-1 * center); // Move bunny to origin
	   // MV.rotate(M_PI / 2, Eigen::Vector3f(1, 0, 0));
	   glUniformMatrix4fv(prog.getUniform("MV"), 1, GL_FALSE, MV.topMatrix().data());
	MV.popMatrix();

	// Enable and bind first curvature array for drawing
	int h_curve1 = prog.getAttribute("curv1");
	GLSL::enableVertexAttribArray(h_curve1);
	glBindBuffer(GL_ARRAY_BUFFER, curve1BufID);
	glVertexAttribPointer(h_curve1, 1, GL_FLOAT, GL_FALSE, 0, 0);

	// Enable and bind second curvature array for drawing
	int h_curve2 = prog.getAttribute("curv2");
	GLSL::enableVertexAttribArray(h_curve2);
	glBindBuffer(GL_ARRAY_BUFFER, curve2BufID);
	glVertexAttribPointer(h_curve2, 1, GL_FLOAT, GL_FALSE, 0, 0);

	// Enable and bind curvature derivative array for drawing
	int h_dCurve = prog.getAttribute("dcurv");
	GLSL::enableVertexAttribArray(h_dCurve);
	glBindBuffer(GL_ARRAY_BUFFER, dCurveBufID);
	glVertexAttribPointer(h_dCurve, 4, GL_FLOAT, GL_FALSE, 0, 0);

	// Enable and bind first curvature derivative array for drawing
	int h_pDir1 = prog.getAttribute("pdir1");
	GLSL::enableVertexAttribArray(h_pDir1);
	glBindBuffer(GL_ARRAY_BUFFER, pDir1BufID);
	glVertexAttribPointer(h_pDir1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// Enable and bind second curvature derivative array for drawing
	int h_pDir2 = prog.getAttribute("pdir2");
	GLSL::enableVertexAttribArray(h_pDir2);
	glBindBuffer(GL_ARRAY_BUFFER, pDir2BufID);
	glVertexAttribPointer(h_pDir2, 3, GL_FLOAT, GL_FALSE, 0, 0);

	bunny.draw(prog.getAttribute("vertPos"), prog.getAttribute("vertNor"), -1);
        
	
   // Draw bunny 2 at right of Origin
 //   R.block<3,3>(0,0) = q1.toRotationMatrix();
 //   MV.pushMatrix();
 //      MV.translate(p1);
 //      MV.multMatrix(R);
	//    MV.translate(-1 * center); // Move bunny to origin
 //      glUniformMatrix4fv(prog.getUniform("MV"), 1, GL_FALSE, MV.topMatrix().data());
	// MV.popMatrix();
	// bunny.draw(prog.getAttribute("vertPos"), prog.getAttribute("vertNor"), -1);
        
   // if (keyToggles[' ']) {
   //    // Draw bunny 3 that moves between bunny 1 and 2
   //    // P(u) = (1 - u)q0 + uq1
   //    Eigen::Vector3f lerp = (1 - alpha) * p0 + alpha * p1;
   //    R.block<3,3>(0,0) = q0.slerp(alpha, q1).toRotationMatrix();

   //    MV.pushMatrix();
   //       MV.translate(lerp);
   //       MV.multMatrix(R);
	  //     MV.translate(-1 * center);
   //       glUniformMatrix4fv(prog.getUniform("MV"), 1, GL_FALSE, MV.topMatrix().data());
   //    MV.popMatrix();
   //    bunny.draw(prog.getAttribute("vertPos"), prog.getAttribute("vertNor"), -1);
   // }
	
	// Unbind the program
	prog.unbind();

	//////////////////////////////////////////////////////
	// Cleanup
	//////////////////////////////////////////////////////
	
	// Pop stacks
	MV.popMatrix();
	P.popMatrix();
	
	// Swap buffer
	glutSwapBuffers();
}

void mouseGL(int button, int state, int x, int y)
{
	int modifier = glutGetModifiers();
	bool shift = modifier & GLUT_ACTIVE_SHIFT;
	bool ctrl  = modifier & GLUT_ACTIVE_CTRL;
	bool alt   = modifier & GLUT_ACTIVE_ALT;
	camera.mouseClicked(x, y, shift, ctrl, alt);
}

void mouseMotionGL(int x, int y)
{
	camera.mouseMoved(x, y);
}

void keyboardGL(unsigned char key, int x, int y)
{
	keyToggles[key] = !keyToggles[key];
	switch(key) {
		case 27:
			// ESCAPE
			exit(0);
			break;
		case 't':
			t = 0.0f;
			break;
	}
}

void timerGL(int value)
{
	glutPostRedisplay();
	glutTimerFunc(20, timerGL, 0);
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitWindowSize(400, 400);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutCreateWindow("Your Name");
	glutMouseFunc(mouseGL);
	glutMotionFunc(mouseMotionGL);
	glutKeyboardFunc(keyboardGL);
	glutReshapeFunc(reshapeGL);
	glutDisplayFunc(drawGL);
	glutTimerFunc(20, timerGL, 0);
	loadScene();
	initGL();
	glutMainLoop();
	return 0;
}
