#version 120

// varying vec3 fragNor;
// varying vec3 fragPos;
// varying vec4 color;
uniform float fSize;

varying float dotProd;
varying float curve;
varying float dCurve;

varying float kr;
varying float dirDeriv;

void main()
{
	// vec3 normal = normalize(fragNor);
	// Map normal in the range [-1, 1] to color in range [0, 1];
	// vec3 color = 0.5*normal + 0.5;
	// gl_FragColor = color;

	//Dot product
	// if (dotProd < 0.35) {
	// 	gl_FragColor = vec4(0, 0, 0, 1);
	// }
	// else {
	// 	gl_FragColor = vec4(1, 1, 1, 1);
	// }

	// base color
	vec4 color = vec4(1.0f, 1.0f, 1.0f, 1.0f); 
	float fz = fSize; //0.00100051;
	
	// use feature size
	float krN = fz*abs(kr); // absolute value to use it in limits
	float dirDerivN = fz * fz * dirDeriv; // two times fz because derivative
	float dirDeriv2 = (dirDerivN-dirDerivN*pow(dotProd, 2.0));

	// compute limits
	float contour = pow(dotProd, 2.0)/krN;
	float sugContour = krN/dirDeriv2;
	// contours
	if(contour < 0.1) {
		color.xyz = min(color.xyz, vec3(contour, contour, contour));
		// color.xyz = min(color.xyz, vec3(0.0, 0.0, 0.0));
	}

	// if (contour < 0.35) {
	// 	gl_FragColor = vec4(0, 0, 0, 1);
	// }
	// else {
	// 	gl_FragColor = vec4(1, 1, 1, 1);
	// }

	// suggestive contours
	else if((sugContour < 0.5) && dirDeriv2 > 0.05) {
	// if((sugContour < 0.5)) { //&& dirDeriv2 > 0.05) {
	// if(dirDeriv2 > 0.05) {
		// color.xyz = min(color.xyz, vec3(sugContour, sugContour, sugContour));
		color.xyz = min(color.xyz, vec3(0.0, 0.0, 0.0));
	}
	gl_FragColor = color;

	// gl_FragColor = vec4(dotProd, dotProd, dotProd, 1);
	// gl_FragColor = vec4(contour, contour, contour, 1);
	// gl_FragColor = vec4(krN, krN, krN, 1);
	// gl_FragColor = vec4(sugContour, sugContour, sugContour, 1);
	// gl_FragColor = vec4(dirDeriv2, dirDeriv2, dirDeriv2, 1);
	// gl_FragColor = vec4(dirDeriv, dirDeriv, dirDeriv, 1);
	// gl_FragColor = vec4(dirDerivN, dirDerivN, dirDerivN, 1);


	//Curvature
	// if (curve < 25 && curve > -25) {
	// 	gl_FragColor = vec4(0, 0, 0, 1);
	// }
	// else {
	// 	gl_FragColor = vec4(1, 1, 1, 1);
	// }

	// gl_FragColor = vec4(curve, curve, curve, 1);

	//Derivative of curvature
	// gl_FragColor = vec4(dCurve, dCurve, dCurve, 1);

	// if (dCurve > 0 ) { //&& curve < 1 && curve > -1) {
	// 	gl_FragColor = vec4(0, 0, 0, 1);
	// }
	// else {
	// 	gl_FragColor = vec4(1, 1, 1, 1);
	// }

	// if (dot(fragNor, fragPos) < 0.5) {
	// 	gl_FragColor = vec4(0, 0, 0, 1);
	// }
	// else {
	// 	gl_FragColor = vec4(1, 1, 1, 1);
	// }

}
