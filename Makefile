CC=g++
CFLAGS=-ansi -pedantic -Wno-deprecated -std=c++0x
INC=-I$(EIGEN3_INCLUDE_DIR)
LIB=-DGL_GLEXT_PROTOTYPES -lglut -lGL -lGLU
LIB_OSX=-framework GLUT -framework OpenGL
TRIMESH_DIR=./trimesh

all:
	$(CC) $(CFLAGS) $(INC) *.cpp *.cc $(TRIMESH_DIR)/*.cc $(LIB) -o contours

osx:
	$(CC) $(CFLAGS) $(INC) *.cpp *.cc $(TRIMESH_DIR)/*.cc $(LIB_OSX) -o contours

clean:
	rm -f *~ *.o a.out

run:
	./contours