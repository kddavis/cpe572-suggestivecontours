#version 120
attribute vec4 vertPos;
attribute vec3 vertNor;
attribute float curv1;
attribute float curv2;
attribute vec3 pdir1;
attribute vec3 pdir2;
attribute vec4 dcurv;

uniform mat4 P;
uniform mat4 MV;

// varying vec3 fragNor;
// varying vec3 fragPos;
// varying vec4 color;
varying float dotProd;
// varying float curve;
// varying float dCurve;
varying float kr;
varying float dirDeriv;

void main()
{
	gl_Position = P * MV * vertPos;
	vec3 normal = (MV * vec4(vertNor, 0.0)).xyz;
	// fragPos  = (P * MV * vertPos).xyz;
	vec3 view = vec3(0, 0, 0) - (MV * vertPos).xyz;
	dotProd = (1.0f / length(view)) * dot(view, normal);

	if(!(dotProd < 0.0f)) {
		// compute kr
		vec3 w = normalize(view - normal * dot(view, normal));
		float u = dot(w, pdir1);
		float v = dot(w, pdir2);
		kr = (curv1 * u * u) + (curv2 * v * v);
		// and DwKr- the directional derivative
		float dwII = (u * u * u * dcurv.x) + (3.0 * u * u * v * dcurv.y) + (3.0 * u * v * v * dcurv.z) + (v * v * v *dcurv.w);
		// extra term due to second derivative
		dirDeriv = dwII + 2.0 * curv1 * curv2 * dotProd/sqrt((1.0 - pow(dotProd, 2.0)));
	}
}
